/*
 * @author Antoine Bouchet
 */
package net.git.classement.raid_classement;


/**
 * The Class activite.
 */
public class activite {

	/** The code represent the primary key of this classe. */
	private String code;
	
	/** The libelle of activite. */
	private String libelle;
	
	/**
	 * Default Constructor of activite.
	 */
	public activite() {
		code = "";
		libelle = "";
	}
	
	/**
	 * Constructor with parameters of activite.
	 *
	 * @param code_ represent the primary key of activity class
	 * @param libelle_ represent the libelle of the activity
	 */
	public activite(String code_, String libelle_) {
		super();
		this.code = code_;
		this.libelle = libelle_;
	}
	
	/**
	 * Gets the libelle.
	 *
	 * @return the libelle of the activity
	 */
	public String getLibelle() {
		return libelle;
	}
	
	/**
	 * Sets the libelle.
	 *
	 * @param libelle_ represent the new libelle of the activity
	 */
	public void setLibelle(String libelle_) {
		this.libelle = libelle_;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code represent the primary key
	 */
	public String getCode() {
		return code;
	}
	

}
