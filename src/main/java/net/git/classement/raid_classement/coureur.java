/*
 * @author Antoine Bouchet
 */
package net.git.classement.raid_classement;

import java.util.Date;


/**
 * The Class coureur.
 */
public class coureur {

	
	/** The licence represent the primary key of coureur class. */
	private String licence;
	
	/** The nom represent the name of coureur class. */
	private String nom;
	
	/** The prenom represent the first name of coureur class. */
	private String prenom;
	
	/** The sexe represent the sexe of coureur class. */
	private String sexe;
	
	/** The nationalite reprsent the nationality of coureur class. */
	private String nationalite;
	
	/** The mail repsent the mail of coureur class. */
	private String mail;
	
	/** The certif med aptitude represent the certif med aptitude of coureur class. */
	private Boolean certif_med_aptitude;
	
	/** The datenaiss represent the date of birth of coureur class. */
	private String datenaiss;
	
	/**
	 * Default Constructor.
	 */
	public coureur() {
		licence = "";
		nom = "";
		prenom = "";
		sexe = "";
		nationalite = "";
		mail = "";
		certif_med_aptitude = false;
		datenaiss = "";
	}
	
	/**
	 * Constructor with parameters.
	 *
	 * @param licence_ represent the primary key of coureur class
	 * @param nom_ represent the name of coureur class
	 * @param prenom_ represent the first name of coureur class
	 * @param sexe_ represent the sexe of coureur class
	 * @param nationalite_ represent the nationality of coureur class
	 * @param mail_ the mail represent the mail of coureur class
	 * @param certif_med_aptitude_ represent the certif med aptitude of coureur class
	 * @param datenaiss_ represent the date of birth of coureur class
	 */
	public coureur(String licence_, String nom_, String prenom_, String sexe_, String nationalite_, String mail_,
			Boolean certif_med_aptitude_, String datenaiss_) {
		super();
		this.licence = licence_;
		this.nom = nom_;
		this.prenom = prenom_;
		this.sexe = sexe_;
		this.nationalite = nationalite_;
		this.mail = mail_;
		this.certif_med_aptitude = certif_med_aptitude_;
		this.datenaiss = datenaiss_;
	}

	/**
	 * Gets the licence.
	 *
	 * @return the primary key of coureur class
	 */
	public String getLicence() {
		return licence;
	}
	
	/**
	 * Gets the nom.
	 *
	 * @return the name of coureur class
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Sets the nom.
	 *
	 * @param nom_ represent the new name of coureur class
	 */
	public void setNom(String nom_) {
		this.nom = nom_;
	}

	/**
	 * Gets the prenom.
	 *
	 * @return the first name of coureur class
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * Sets the prenom.
	 *
	 * @param prenom_ represent the new first name of coureur class
	 */
	public void setPrenom(String prenom_) {
		this.prenom = prenom_;
	}

	/**
	 * Gets the sexe.
	 *
	 * @return the sexe of coureur class
	 */
	public String getSexe() {
		return sexe;
	}

	/**
	 * Sets the sexe.
	 *
	 * @param sexe_ represent the new sexe of coureur class
	 */
	public void setSexe(String sexe_) {
		this.sexe = sexe_;
	}

	/**
	 * Gets the nationalite.
	 *
	 * @return the nationalite of coureur class
	 */
	public String getNationalite() {
		return nationalite;
	}

	/**
	 * Sets the nationalite.
	 *
	 * @param nationalite_ represent the new nationality of coureur class
	 */
	public void setNationalite(String nationalite_) {
		this.nationalite = nationalite_;
	}

	/**
	 * Gets the mail.
	 *
	 * @return the mail of coureur class
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * Sets the mail.
	 *
	 * @param mail_ represent the new mail of coureur class
	 */
	public void setMail(String mail_) {
		this.mail = mail_;
	}

	/**
	 * Gets the certif med aptitude.
	 *
	 * @return the certif med aptitude of coureur class
	 */
	public Boolean getCertif_med_aptitude() {
		return certif_med_aptitude;
	}

	/**
	 * Sets the certif med aptitude.
	 *
	 * @param certif_med_aptitude_ represent the new certif med aptitude of coureur class
	 */
	public void setCertif_med_aptitude(Boolean certif_med_aptitude_) {
		this.certif_med_aptitude = certif_med_aptitude_;
	}

	/**
	 * Gets the datenaiss.
	 *
	 * @return the date of birth of coureur class
	 */
	public String getDatenaiss() {
		return datenaiss;
	}

	/**
	 * Sets the datenaiss.
	 *
	 * @param datenaiss_ represent the date of birth of coureur class
	 */
	public void setDatenaiss(String datenaiss_) {
		this.datenaiss = datenaiss_;
	}
}
