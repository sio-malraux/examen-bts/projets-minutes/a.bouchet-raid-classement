/*
 * @author Antoine Bouchet
 */
package net.git.classement.raid_classement;
import net.git.classement.raid_classement.activite;
import net.git.classement.raid_classement.raid;


/**
 * The Class activite_raid.
 */
public class activite_raid {

	/** The code raid represent the foreign key of raid class. */
	private String code_raid;
	
	/** The code activite represent the foreign key of activity class. */
	private String code_activite;
	
	/** The object activite represent new activity. */
	activite object_activite = new activite();
	
	/** The object raid represent new raid. */
	raid object_raid = new raid();
	
	/**
	 * Constructor with parameters of activite_raid.
	 *
	 * @param code_raid_ represent the primary key of raid class
	 * @param code_activite_ represent the primary key of activity class
	 */
	public activite_raid(String code_raid_, String code_activite_) {
		super();
		this.code_raid = code_raid_;
		this.code_activite = code_activite_;
	}

	/**
	 * Gets the code raid.
	 *
	 * @return the foreign key of raid class
	 */
	public String getCode_raid() {
		return code_raid;
	}

	/**
	 * Gets the code activite.
	 *
	 * @return the foreign key of activity class
	 */
	public String getCode_activite() {
		return code_activite;
	}	
}
