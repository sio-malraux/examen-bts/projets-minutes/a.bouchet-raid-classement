/*
 * @author Antoine Bouchet
 */
package net.git.classement.raid_classement;
import net.git.classement.raid_classement.raid;
import net.git.classement.raid_classement.departement;

// TODO: Auto-generated Javadoc
/**
 * The Class traverser.
 */
public class traverser {

	/** The code raid represent the foreign key of raid class. */
	private String code_raid;
	
	/** The numero dept represent the foreign key of departement class. */
	private int numero_dept;
	
	/** The object raid represent the foreign key of raid class. */
	raid object_raid = new raid();
	
	/** The object dept represent new object of departement class. */
	departement object_dept = new departement();
	
	
	/**
	 * Constructor with parameters 
	 *
	 * @param code_raid_ represent the primary key of traverser class
	 * @param numero_dept_ represent the primary key of traverser class
	 */
	public traverser(String code_raid_, int numero_dept_) {
		super();
		this.code_raid = code_raid_;
		this.numero_dept = numero_dept_;
	}



	/**
	 * Gets the code raid.
	 *
	 * @return the primary key of traverser class also foreign key of raid class
	 */
	public String getCode_raid() {
		return code_raid;
	}



	/**
	 * Gets the numero dept.
	 *
	 * @return the primary key of traverser class also foreign key of departement class
	 */
	public int getNumero_dept() {
		return numero_dept;
	}
	
}
