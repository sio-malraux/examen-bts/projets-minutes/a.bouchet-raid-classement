/*
 * @author Antoine Bouchet
 */
package net.git.classement.raid_classement;
import java.util.Date;
import net.git.classement.raid_classement.raid;

// TODO: Auto-generated Javadoc
/**
 * The Class equipe.
 */
public class equipe {

	/** The numero represent the primary key of equipe class. */
	private int numero;
	
	/** The code raid represent the foreign key of equipe class. */
	private String code_raid;
	
	/** The nom represent the name of equipe class. */
	private String nom;
	
	/** The dateinscription represent registration date of equipe class. */
	private String dateinscription;
	
	/** The temps global represent overall time of equipe class. */
	private int temps_global;
	
	/** The classement represent the classement of equipe class. */
	private int classement;
	
	/** The penalities bonif represent the penalities bonif of equipe class. */
	private int penalities_bonif;
	
	/** The object raid represent the new object of raid class. */
	raid object_raid = new raid();

	
	/**
	 * Default Constructor.
	 */
	// Constructor
	public equipe() {
		numero = 0;
		code_raid = "";
		nom = "";
		dateinscription = "";
		temps_global = 0;
		classement = 0;
		penalities_bonif = 0;
	}
	
	/**
	 * Constructor with parameters.
	 *
	 * @param numero_ represent the primary key of equipe class
	 * @param code_raid_ represent the foreign key of raid class
	 * @param nom_ represent the name of equipe class
	 * @param dateinscription_ represent registration date of equipe class
	 * @param temps_global_ represent overall time of equipe class
	 * @param classement_ represent the classement of equipe class
	 * @param penalities_bonif_ represent the penalities bonif of equipe class
	 */
	// Constructor with parameters
	public equipe(int numero_, String code_raid_, String nom_, String dateinscription_, int temps_global_, int classement_,
			int penalities_bonif_) {
		super();
		this.numero = numero_;
		this.code_raid = code_raid_;
		this.nom = nom_;
		this.dateinscription = dateinscription_;
		this.temps_global = temps_global_;
		this.classement = classement_;
		this.penalities_bonif = penalities_bonif_;
	}
	
	/**
	 * Gets the numero.
	 *
	 * @return the primary key of equipe class
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * Gets the code raid.
	 *
	 * @return the foreign key of raid class
	 */
	public String getCode_raid() {
		return code_raid;
	}


	/**
	 * Gets the nom.
	 *
	 * @return the name of equipe class
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Sets the nom.
	 *
	 * @param nom_ represent the new name of equipe class
	 */
	public void setNom(String nom_) {
		this.nom = nom_;
	}

	/**
	 * Gets the dateinscription.
	 *
	 * @return the registration date of equipe class
	 */
	public String getDateinscription() {
		return dateinscription;
	}

	/**
	 * Sets the dateinscription.
	 *
	 * @param dateinscription_ represent registration date of equipe class
	 */
	public void setDateinscription(String dateinscription_) {
		this.dateinscription = dateinscription_;
	}

	/**
	 * Gets the temps global.
	 *
	 * @return the overall time of equipe class
	 */
	public int getTemps_global() {
		return temps_global;
	}

	/**
	 * Sets the temps global.
	 *
	 * @param temps_global_ represent the new overall time of equipe class
	 */
	public void setTemps_global(int temps_global_) {
		this.temps_global = temps_global_;
	}

	/**
	 * Gets the classement.
	 *
	 * @return the classement of equipe class
	 */
	public int getClassement() {
		return classement;
	}

	/**
	 * Sets the classement.
	 *
	 * @param classement_ represent the classement of equipe class
	 */
	public void setClassement(int classement_) {
		this.classement = classement_;
	}

	/**
	 * Gets the penalities bonif.
	 *
	 * @return the penalities bonif of equipe class
	 */
	public int getPenalities_bonif() {
		return penalities_bonif;
	}

	/**
	 * Sets the penalities bonif.
	 *
	 * @param penalities_bonif_ represent the new penalities bonif of equipe class
	 */
	public void setPenalities_bonif(int penalities_bonif_) {
		this.penalities_bonif = penalities_bonif_;
	}

}
