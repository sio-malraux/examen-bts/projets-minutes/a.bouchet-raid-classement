/*
 * @author Antoine Bouchet 
 */
package net.git.classement.raid_classement;

/**
 * The Class departement.
 */
public class departement {

	/** The numero represent the primary key of departement class. */
	private int numero;
	
	/** The nom represent the name to departement. */
	private String nom;
	
	/**
	 * Default Constructor of departement.
	 */
	public departement() {
		numero = 0;
		nom = "";
	}
	
	/**
	 * Constructor with parameters.
	 *
	 * @param numero_ represent the primary key of departement
	 * @param nom_ represent the name to departement
	 */
	public departement(int numero_, String nom_) {
		super();
		this.numero = numero_;
		this.nom = nom_;
	}

	/**
	 * Gets the nom.
	 *
	 * @return the name to departement
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Sets the nom.
	 *
	 * @param nom_ represent the new name to departement 
	 */
	public void setNom(String nom_) {
		this.nom = nom_;
	}

	/**
	 * Gets the numero.
	 *
	 * @return the primary key of departement class
	 */
	public int getNumero() {
		return numero;
	}

	
}
