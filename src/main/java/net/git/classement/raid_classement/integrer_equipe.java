/*
 * @author Antoine Bouchet
 */
package net.git.classement.raid_classement;

import net.git.classement.raid_classement.equipe;
import net.git.classement.raid_classement.raid;
import net.git.classement.raid_classement.coureur;


/**
 * The Class integrer_equipe.
 */
public class integrer_equipe {

	/** The numero equipe represent the foreign key of equipe class also it's a primary key of integrer_equipe class. */
	private int numero_equipe;
	
	/** The code raid represent the foreign key of raid class also it's a primary key of integrer_equipe class. */
	private String code_raid;
	
	/** The licence represent the foreign key of coureur class also it's a primary key of integrer_equipe class. */
	private String licence;
	
	/** The temps individuel represent the indivdual time for integrer_equipe class. */
	private int temps_individuel;
	
	/** The num dossard represent the bib number for integrer_equipe class. */
	private String num_dossard;
	
	
	/** The object equipe represent the new object for equipe class. */
	equipe object_equipe = new equipe();
	
	/** The object raid represent the new object for raid class. */
	raid object_raid = new raid();
	
	/** The object coureur represent the new object for coureur class. */
	coureur object_coureur = new coureur();
	
	
	/**
	 * Default Constructor.
	 */
	public integrer_equipe() {
		numero_equipe = 0;
		code_raid = "";
		licence = "";
		temps_individuel = 0;
		num_dossard = "";
	}
	
	/**
	 * Constructor with parameters.
	 *
	 * @param numero_equipe_ represent the foreign key of equipe class also it's a primary key of integrer_equipe class
	 * @param code_raid_ represent the foreign key of raid class also it's a primary key of integrer_equipe class
	 * @param licence_ represent the foreign key of coureur class also it's a primary key of integrer_equipe class
	 * @param temps_individuel_ represent the indivdual time for integrer_equipe class
	 * @param num_dossard_ represent the new object for coureur class
	 */
	public integrer_equipe(int numero_equipe_, String code_raid_, String licence_, int temps_individuel_,
			String num_dossard_) {
		super();
		this.numero_equipe = numero_equipe_;
		this.code_raid = code_raid_;
		this.licence = licence_;
		this.temps_individuel = temps_individuel_;
		this.num_dossard = num_dossard_;
	}

	/**
	 * Gets the temps individuel.
	 *
	 * @return the individual time of integrer_equipe class
	 */
	public int getTemps_individuel() {
		return temps_individuel;
	}

	/**
	 * Sets the temps individuel.
	 *
	 * @param temps_individuel_ represent the new individual time for integrer_equipe class
	 */
	public void setTemps_individuel(int temps_individuel_) {
		this.temps_individuel = temps_individuel_;
	}

	/**
	 * Gets the num dossard.
	 *
	 * @return the bib number of integrer_equipe class
	 */
	public String getNum_dossard() {
		return num_dossard;
	}

	/**
	 * Sets the num dossard.
	 *
	 * @param num_dossard_ represent the new bib number of integrer_equipe class
	 */
	public void setNum_dossard(String num_dossard_) {
		this.num_dossard = num_dossard_;
	}

	/**
	 * Gets the numero equipe.
	 *
	 * @return the foreign key of equipe class also it's a primary key of integrer_equipe class
	 */
	public int getNumero_equipe() {
		return numero_equipe;
	}

	/**
	 * Gets the code raid.
	 *
	 * @return the foreign key of raid class also it's a primary key of integrer_equipe class
	 */
	public String getCode_raid() {
		return code_raid;
	}

	/**
	 * Gets the licence.
	 *
	 * @return the foreign key of coureur class also it's a primary key of integrer_equipe class
	 */
	public String getLicence() {
		return licence;
	}
	
	
}
