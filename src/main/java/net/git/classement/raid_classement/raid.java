/*
 * @author Antoine Bouchet
 */
package net.git.classement.raid_classement;

/**
 * The Class raid.
 */
public class raid {
	
	/** The code represent primary key of raid class. */
	private String code;
	
	/** The nom represent the name to raid. */
	private String nom;
	
	/** The datedebut represent the beginning date to raid  . */
	private String datedebut;
	
	/** The ville represent the city to raid. */
	private String ville;
	
	/** The region represent the region to raid. */
	private String region;
	
	/** The nb maxi par equipe represent the maximum number of people per team. */
	private String nb_maxi_par_equipe;
	
	/** The montant inscription represent registration amount. */
	private Double montant_inscription;
	
	/** The nb femmes represent number of women. */
	private int nb_femmes;
	
	/** The duree maxi represent the max duration. */
	private int duree_maxi;
	
	/** The age minimum represent minimum age. */
	private int age_minimum;
	
	
	/**
	 * Default Constructor.
	 */
	// Constructor
	public raid() {
		code = "";
		nom = "";
		datedebut = "";
		ville = "";
		region = "";
		nb_maxi_par_equipe = "";
		montant_inscription = 0.0;
		nb_femmes = 0;
		duree_maxi = 0;
		age_minimum = 0;		
	}
	
	/**
	 * Constructor with parameters.
	 *
	 * @param code_ represent the primary key of raid class
	 * @param nom_ represent the name of raid class
	 * @param datedebut_ represent the beginning date of raid class
	 * @param ville_ represent the city of raid class
	 * @param region_ represent the region of raid class
	 * @param nb_maxi_par_equipe_ represent the maximum number of people per team of raid class
	 * @param montant_inscription_ represent registration amount of raid class
	 * @param nb_femmes_ represent the number of women of raid class
	 * @param duree_maxi_ represent the max duration of raid class
	 * @param age_minimum_ represent minimum age of raid class
	 */
	// Constructor with parameters
	public raid(String code_, String nom_, String datedebut_, String ville_, String region_, 
				String nb_maxi_par_equipe_, Double montant_inscription_, int nb_femmes_, int duree_maxi_, int age_minimum_) {
		
		super();
		this.code = code_;
		this.nom = nom_;
		this.datedebut = datedebut_;
		this.ville = ville_;
		this.region = region_;
		this.nb_maxi_par_equipe = nb_maxi_par_equipe_;
		this.montant_inscription = montant_inscription_;
		this.nb_femmes = nb_femmes_;
		this.duree_maxi = duree_maxi_;
		this.age_minimum = age_minimum_;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the primary key of raid class
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * Gets the nom.
	 *
	 * @return the name to raid
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * Sets the nom.
	 *
	 * @param nom_ represent the new name to raid
	 */
	public void setNom(String nom_) {
		this.nom = nom_;
	}
	
	/**
	 * Gets the datedebut.
	 *
	 * @return the beginning date to raid
	 */
	public String getDatedebut() {
		return datedebut;
	}
	
	/**
	 * Sets the datedebut.
	 *
	 * @param datedebut_ represent the new beginning date
	 */
	public void setDatedebut(String datedebut_) {
		this.datedebut = datedebut_;
	}
	
	/**
	 * Gets the ville.
	 *
	 * @return the city to raid
	 */
	public String getVille() {
		return ville;
	}
	
	/**
	 * Sets the ville.
	 *
	 * @param ville_ represent the new city to raid
	 */
	public void setVille(String ville_) {
		this.ville = ville_;
	}
	
	/**
	 * Gets the region.
	 *
	 * @return the region to raid
	 */
	public String getRegion() {
		return region;
	}
	
	/**
	 * Sets the region.
	 *
	 * @param region_ represent the new region to raid
	 */
	public void setRegion(String region_) {
		this.region = region_;
	}
	
	/**
	 * Gets the nb maxi par equipe.
	 *
	 * @return the maximum number of people per team of raid class 
	 */
	public String getNb_maxi_par_equipe() {
		return nb_maxi_par_equipe;
	}
	
	/**
	 * Sets the nb maxi par equipe.
	 *
	 * @param nb_maxi_par_equipe_ represent the new maximum number of people per team of raid class
	 */
	public void setNb_maxi_par_equipe(String nb_maxi_par_equipe_) {
		this.nb_maxi_par_equipe = nb_maxi_par_equipe_;
	}
	
	/**
	 * Gets the montant inscription.
	 *
	 * @return the registration amount to raid
	 */
	public Double getMontant_inscription() {
		return montant_inscription;
	}
	
	/**
	 * Sets the montant inscription.
	 *
	 * @param montant_inscription_ represent the new registration amount to raid
	 */
	public void setMontant_inscription(Double montant_inscription_) {
		this.montant_inscription = montant_inscription_;
	}
	
	/**
	 * Gets the nb femmes.
	 *
	 * @return the number women to raid
	 */
	public int getNb_femmes() {
		return nb_femmes;
	}
	
	/**
	 * Sets the nb femmes.
	 *
	 * @param nb_femmes_ represent the new number of women to raid
	 */
	public void setNb_femmes(int nb_femmes_) {
		this.nb_femmes = nb_femmes_;
	}
	
	/**
	 * Gets the duree maxi.
	 *
	 * @return the max duration to raid
	 */
	public int getDuree_maxi() {
		return duree_maxi;
	}
	
	/**
	 * Sets the duree maxi.
	 *
	 * @param duree_maxi_ represent the new max duration to raid
	 */
	public void setDuree_maxi(int duree_maxi_) {
		this.duree_maxi = duree_maxi_;
	}
	
	/**
	 * Gets the age minimum.
	 *
	 * @return the minimum age
	 */
	public int getAge_minimum() {
		return age_minimum;
	}
	
	/**
	 * Sets the age minimum.
	 *
	 * @param age_minimum_ represent the new minimum age for the raid
	 */
	public void age_minimum(int age_minimum_) {
		this.age_minimum = age_minimum_;
	}
}
